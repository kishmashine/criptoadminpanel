import React from 'react';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom'; // Импортируем Routes вместо Switch
import Sidebar from './components/Sidebar/Sidebar';
import Dashboard from './components/Dashboard/Dashboard';
import Limits from './components/Limits/Limits';
import Transactions from './components/Transactions/Transactions';
import User from './components/User/User';
import Stocks from './components/Stocks/Stocks';
import s from './App.module.css'
import { MantineProvider } from '@mantine/core';
import '@mantine/charts/styles.css';

function App() {
  return (
    <Router>
      <MantineProvider>
      <div className={s.main}>
        <div>
          <Sidebar />
        </div>
        <div className={s.content}>
          <Routes>
            <Route path="/dashboard" element={<Dashboard />} />
            <Route path="/limits" element={<Limits />} />
            <Route path="/transactions" element={<Transactions />} />
            <Route path="/user" element={<User />} />
            <Route path="/stocks" element={<Stocks />} />
          </Routes>
        </div>
      </div>
      </MantineProvider>
    </Router>
  );
}

export default App;
