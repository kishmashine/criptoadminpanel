import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import s from './Sidebar.module.css';
import logo3 from './../../assets/Untitled_logo_2_free-file 4.png';

const links = [
  { to: '/dashboard', text: 'Панель управления' },
  { to: '/limits', text: 'Лимиты' },
  { to: '/transactions', text: 'Транзакции' },
  { to: '/user', text: 'Пользователь' },
  { to: '/stocks', text: 'Акции' }
];

const Sidebar = () => {
  const [activeLink, setActiveLink] = useState(null);

  const handleLinkClick = (link) => {
    setActiveLink(link);
  };
  
  return (
    <div className={s.sidebar}>
      <div className={s.logo}>
      <Link to="/" className={s.logoLink}>
          <img src={logo3} alt="logo" />
        </Link>
      </div>
      <ul className={s.sidebarList}>
        {links.map((link, index) => (
          <li key={index} className={s.sidebarItem}>
            <Link
              to={link.to}
              className={`${s.sidebarLink} ${activeLink === link.to ? s.active : ''}`}
              onClick={() => handleLinkClick(link.to)} >
              {link.text}
            </Link>
          </li>
        ))}
      </ul>
    </div>
  );
}

export default Sidebar;

