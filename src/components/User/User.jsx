import React from 'react';
import s from './User.module.css';
const User = () => {
  return (
    <div className={s.userContainer}>
      <div className={s.title}>
        <h2>Пользователь</h2>
        <h5 className={s.h5}>
          <span className={s.outerColor1}>Главная /</span>
          <span className={s.outerColor2}>Пользователь</span>
        </h5>
      </div>
      <div className={s.titleUserInfoContainer}>

        <div className={s.titleUserInfo}>
          <p className={s.userInfoText}>Email пользователя:</p>
          <p className={s.userInfoText2}>Birma0@gmail.com</p>
        </div>

        <div className={s.titleUserInfo}>
          <p className={s.userInfoText}>Адрес кошелька:</p>
          <p className={s.userInfoText2}>1A1zP1eP5QGefi2DMPTfTL5SLmv7DivfNa</p>
        </div>

        <div className={s.titleUserInfo}>
          <p className={s.userInfoText}>Имя:</p>
          <p className={s.userInfoText2}>Bober</p>
        </div>

        <div className={s.titleUserInfo}>
          <p className={s.userInfoText}>Тип кошелька:</p>
          <p className={s.userInfoText2}>MetaMask</p>
        </div>

        <div className={s.titleUserInfo}>
          <p className={s.userInfoText}>Количество акций:</p>
          <p className={s.userInfoText2}>395</p>
        </div>

        <div className={s.titleUserInfo}>
          <p className={s.userInfoText}>Дата Реестрации:</p>
          <p className={s.userInfoText2}>15.03.2024</p>
        </div>


      </div>
      <div className={s.approveWraper}>
        <div className={s.titleApprove}>
          <h2>Транзакция ожидающая Ампрува</h2>
        </div>
        <div className={s.approveInfoContainer}>
          <div className={s.approveBlokInfo}>
            <div className={s.aproveInfoTitleBlock}>
              <p>Пользователи</p>
              <p>Дата вывода</p>
              <p>Сума вывода</p>
            </div>

            <div className={s.aproveUserInfoBlock}>
              <p >Birma0@gmail.com</p>
              <p className={s.pInput}>19/03/2024</p>
              <p className={s.pOutput}>$5639</p>
            </div>
          </div>

          <div className={s.approveBlokButton}>
            <div className={s.buttonContainer}><button className={s.button}>Розрешить</button></div>
            <div className={s.buttonContainer}><button className={s.button2}>Отклонить</button></div>
          </div>
        </div>
      </div>
      <div className={s.allTransaktionContainerWraper}>
        <div className={s.titleAllTransaktion}>
          <h2 >Все транзакции</h2>
        </div>
        <div className={s.allTransaktionContainer}>
          <div className={s.allInput}>
            <div className={s.titleAllInputInfo}><p>Ввел</p></div>
            <div className={s.blockInfo}>

              <div className={s.titleInfoDiv}>
                <div className={s.titleInfoDivP}><p>Дата вывода</p></div>
                <div className={s.titleInfoDivP}><p>Сума вывода</p></div>
              </div>

              <div className={s.infoDiv}>
                <div className={s.meaningData}>
                  <p>19/03/2024</p>
                  <p>20:45</p>
                </div>
                <div className={s.meaningSuma}><p>$5639</p></div>
              </div>

            </div>
            <div className={s.blockInfo}>

              <div className={s.titleInfoDiv}>
                <div className={s.titleInfoDivP}><p>Дата вывода</p></div>
                <div className={s.titleInfoDivP}><p>Сума вывода</p></div>
              </div>

              <div className={s.infoDiv}>
                <div className={s.meaningData}>
                  <p>19/03/2024</p>
                  <p>20:45</p>
                </div>
                <div className={s.meaningSuma}><p>$5639</p></div>
              </div>

            </div>
            <div className={s.blockInfo}>

              <div className={s.titleInfoDiv}>
                <div className={s.titleInfoDivP}><p>Дата вывода</p></div>
                <div className={s.titleInfoDivP}><p>Сума вывода</p></div>
              </div>

              <div className={s.infoDiv}>
                <div className={s.meaningData}>
                  <p>19/03/2024</p>
                  <p>20:45</p>
                </div>
                <div className={s.meaningSuma}><p>$5639</p></div>
              </div>

            </div>
          </div>
          <div className={s.allOutput}>
            <div className={s.titleAllInputInfo2}><p>Вивел</p></div>
            <div className={s.blockInfo2}>

              <div className={s.titleInfoDiv}>
                <div className={s.titleInfoDivP}><p>Дата вывода</p></div>
                <div className={s.titleInfoDivP}><p>Сума вывода</p></div>
              </div>

              <div className={s.infoDiv}>
                <div className={s.meaningData}>
                  <p>19/03/2024</p>
                  <p>20:45</p>
                </div>
                <div className={s.meaningSuma}><p>$5639</p></div>
              </div>

            </div>
            <div className={s.blockInfo2}>

              <div className={s.titleInfoDiv}>
                <div className={s.titleInfoDivP}><p>Дата вывода</p></div>
                <div className={s.titleInfoDivP}><p>Сума вывода</p></div>
              </div>

              <div className={s.infoDiv}>
                <div className={s.meaningData}>
                  <p>19/03/2024</p>
                  <p>20:45</p>
                </div>
                <div className={s.meaningSuma}><p>$5639</p></div>
              </div>

            </div>
            <div className={s.blockInfo2}>

              <div className={s.titleInfoDiv}>
                <div className={s.titleInfoDivP}><p>Дата вывода</p></div>
                <div className={s.titleInfoDivP}><p>Сума вывода</p></div>
              </div>

              <div className={s.infoDiv}>
                <div className={s.meaningData}>
                  <p>19/03/2024</p>
                  <p>20:45</p>
                </div>
                <div className={s.meaningSuma}><p>$5639</p></div>
              </div>

            </div>
          </div>
          <div className={s.allRejected}>
            <div className={s.titleAllInputInfo3}><p>Отклоненно</p></div>
            <div className={s.blockInfo3}>

              <div className={s.titleInfoDiv}>
                <div className={s.titleInfoDivP}><p>Дата вывода</p></div>
                <div className={s.titleInfoDivP}><p>Сума вывода</p></div>
              </div>

              <div className={s.infoDiv}>
                <div className={s.meaningData}>
                  <p>19/03/2024</p>
                  <p>20:45</p>
                </div>
                <div className={s.meaningSuma}><p>$5639</p></div>
              </div>

            </div>
            <div className={s.blockInfo3}>

              <div className={s.titleInfoDiv}>
                <div className={s.titleInfoDivP}><p>Дата вывода</p></div>
                <div className={s.titleInfoDivP}><p>Сума вывода</p></div>
              </div>

              <div className={s.infoDiv}>
                <div className={s.meaningData}>
                  <p>19/03/2024</p>
                  <p>20:45</p>
                </div>
                <div className={s.meaningSuma}><p>$5639</p></div>
              </div>

            </div>
            <div className={s.blockInfo3}>

              <div className={s.titleInfoDiv}>
                <div className={s.titleInfoDivP}><p>Дата вывода</p></div>
                <div className={s.titleInfoDivP}><p>Сума вывода</p></div>
              </div>

              <div className={s.infoDiv}>
                <div className={s.meaningData}>
                  <p>19/03/2024</p>
                  <p>20:45</p>
                </div>
                <div className={s.meaningSuma}><p>$5639</p></div>
              </div>

            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default User;