import React, { useState } from 'react';
import s from './Transactions.module.css';

const Transactions = () => {
  const [transactions, setTransactions] = useState([
    {
      user: 'Birma@@gmail.com',
      walletAddress: '1A12P1P50Ge1120MFTFTL55Lv701v Na',
      date: '19/03/2024',
      amount: '$5639',
    },
    {
      user: 'Birmaß@gmail.com',
      walletAddress: '1A1zP1P500ef120NPT/TLSSLv7DsvfNa',
      date: '19/03/2024',
      amount: '$5639',
    },
    {
      user: 'Birma0@gmail.com',
      walletAddress: '241218P500dg50NPT/TLSBLAvDvfNa',
      date: '19/03/2024',
      amount: '$5639',
    },
    {
      user: 'Birma0@gmail.com',
      walletAddress: '141218P500ef120NPT/TLSBLAvDvfNa',
      date: '19/03/2024',
      amount: '$5639',
    },

  ]);

  return (
    <div className={s.container}>
      <div className={s.title}>
        <h2>Транзакции</h2>
        <h5 className={s.h5}>
          <span className={s.outerColor1}>Главная /</span>
          <span className={s.outerColor2}>Транзакции</span>
        </h5>
      </div>
      <table className={s.table}>
        <thead>
          <tr>
            <th className={s.thUser}>Пользователи</th>
            <th className={s.thAddress}>Адрес кошелька</th>
            <th className={s.thDate}>Дата вывода</th>
            <th className={s.thAmount}>Сума вывода</th>
            <th className={s.thDetails}>Детали</th>
          </tr>
        </thead>
        <tbody>
          {transactions.map((transaction) => (
            <tr key={transaction.id}>
              <td className={s.tdUser}>{transaction.user}</td>
              <td className={s.tdAddress}>{transaction.walletAddress}</td>
              <td className={s.tdDate}>{transaction.date}</td>
              <td className={s.tdAmount}>{transaction.amount}</td>
              <td className={s.tdDetails}>
                <button className={s.detailsButton}>Смотреть</button>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
};

export default Transactions;