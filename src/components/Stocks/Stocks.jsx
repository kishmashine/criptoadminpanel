import React from 'react';
import s from './Stocks.module.css'
import { AreaChart } from '@mantine/charts';


const data = [
  {
    date: 'Mar 22',
    Apples: 2890,
    Oranges: 2338,
    Tomatoes: 2452,
  },
  {
    date: 'Mar 23',
    Apples: 2756,
    Oranges: 2103,
    Tomatoes: 2402,
  },
  {
    date: 'Mar 24',
    Apples: 3322,
    Oranges: 986,
    Tomatoes: 1821,
  },
  {
    date: 'Mar 25',
    Apples: 3470,
    Oranges: 2108,
    Tomatoes: 2809,
  },
  {
    date: 'Mar 26',
    Apples: 3129,
    Oranges: 1726,
    Tomatoes: 2290,
  },
];

function Stocks() {
  return (
    <div className={s.stocksContainerWraper}>
      <div className={s.title}>
        <h2>Акции</h2>
        <h5 className={s.h5}>
          <span className={s.outerColor1}>Главная /</span>
          <span className={s.outerColor2}>Акции</span>
        </h5>
      </div>
      <div className={s.stocksContainer}>


        <div className={s.stocksInputContainer}>
          <div className={s.currentSharePriceContainer}>
            <div className={s.titlePrice}><p>Текущий курс акции <hr /> </p></div>
            <div className={s.stockPrice}><p>$500</p></div>
          </div>

          <div className={s.upInputContainer}>
            <div className={s.inputUp}>
              <input type="text"
                placeholder='+4%' />
            </div>
            <div className={s.buttonUpContainer}>
              <button className={s.buttonUp}>Повысить</button>
            </div>
          </div>

          <div className={s.downInputContainer}>
            <div className={s.inputUp}>
              <input type="text"
                placeholder='-4%' />
            </div>
            <div className={s.buttonUpContainer}>
              <button className={s.buttonUp}>Понизить</button>
            </div>
          </div>

          <div className={s.variantPercentContainer}>
            <button className={s.buttPrcent}>+2%</button>
            <button className={s.buttPrcent}>+3%</button>
            <button className={s.buttPrcent}>+4%</button>
          </div>
          <div className={s.buttonSubmit}>
            <button className={s.buttSub}>Submit</button>
          </div>
        </div>

        <div className={s.stocksScheduleContainer}>
          <div className={s.titleSchedule}><p>Обзор</p></div>





          <div className={s.LinearChart}>


            <AreaChart
              h={300}
              data={data}
              dataKey="date"
              series={[
                { name: 'Apples', color: 'indigo.6' },
                { name: 'Oranges', color: 'blue.6' },
                { name: 'Tomatoes', color: 'teal.6' },
              ]}
              curveType="step"
            />




            {/* <LineChart width={500} height={300} data={data}>
              <Line type="monotone" dataKey="value" stroke="#8884d8" />
              <CartesianGrid stroke="#ccc" />
              <XAxis dataKey="name" />
              <YAxis />
              <Tooltip />
              <Legend />
            </LineChart> */}
          </div>
        </div>

      </div>
    </div>

  );
}
export default Stocks