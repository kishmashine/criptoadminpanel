import React, { useState } from 'react';
import s from './Limits.module.css';
import dolar from './../../assets/Dollar_Sign 4.png'

const Limits = () => {
  return (
    <div className={s.limits}>
      <div className={s.title}>
        <h2>Лимиты</h2>
        <h5 className={s.h5}>
          <span className={s.outerColor1}>Главная /</span>
          <span className={s.outerColor2}>Лимиты</span>
        </h5>
      </div>
      <div className={s.blocksLimits}>

        <div className={s.blockInputLimit}>
          <div className={s.inputLimit}>
            <h4 className={s.titleInput}>Дневной лимит ввода средств</h4>
            <div className={s.limitsBoardInfo}>
              <div className={s.sumaLimit}><p>$700</p></div>
              <div className={s.dolarImg}><img src={dolar} alt="dolar" /></div>
            </div>
          </div>
          <div className={s.input}>
            <input type="text" className={s.inputField} placeholder='$10' />
          </div>
          <div className={s.inputButton}>
            <button
              className={s.limitButton}
              onClick={() => ({})}>Изменить</button>
          </div>
        </div>

        <div className={s.blockOutputLimit}>
          <div className={s.outputLimit}>
            <h4 className={s.titleOutput}>Дневной лимит вывода средств</h4>
            <div className={s.limitsBoardInfo}>
              <div className={s.sumaLimit}><p>$500</p></div>
              <div className={s.dolarImg}><img src={dolar} alt="dolar" /></div>
            </div>
          </div>
          <div className={s.output}>
            <input type="text" className={s.outputField} placeholder='$10' />
          </div>
          <div className={s.outputButton}>
            <button
              className={s.limitButton}
              onClick={() => ({})}>Изменить</button>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Limits