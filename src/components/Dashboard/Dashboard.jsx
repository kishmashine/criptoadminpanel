import React from 'react';
import s from './Dashboard.module.css'; 

const Dashboard = () => {
  return (
    <div className={s.dashboardContainer}>
      <div className={s.title}>
        <h2>Панель управления</h2>
        <h5 className={s.h5}>
          <span className={s.outerColor1}>Главная /</span>
          <span className={s.outerColor2}>Панель управления</span>
        </h5>
      </div>
      <div className={s.dashboardContent}>
        <div className={s.dashboardSection}>
          <h3>Управление пользователями</h3>
          <ul>
            <li>Добавить пользователя</li>
            <li>Удалить пользователя</li>
            <li>Редактировать пользователя</li>
          </ul>
        </div>
        <div className={s.dashboardSection}>
          <h3>Управление контентом</h3>
          <ul>
            <li>Добавить новость</li>
            <li>Удалить новость</li>
            <li>Редактировать новость</li>
          </ul>
        </div>
        <div className={s.dashboardSection}>
          <h3>Настройки сайта</h3>
          <ul>
            <li>Изменить тему</li>
            <li>Настроить рассылку</li>
            <li>Управление правами доступа</li>
          </ul>
        </div>
      </div>
    </div>
  );
}

export default Dashboard;
